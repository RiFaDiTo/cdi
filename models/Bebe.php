<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bebe".
 *
 * @property integer $idBebe
 * @property string $nombre
 * @property string $apPaterno
 * @property string $apMaterno
 * @property string $fechnac
 * @property string $observaciones
 * @property string $colonia
 * @property string $delegacion
 * @property string $cp
 * @property string $sexo
 * @property integer $status
 * @property string $entero
 *
 * @property BebeContacto[] $bebeContactos
 * @property Citas[] $citas
 * @property Llamadas[] $llamadas
 * @property Obscasa[] $obscasas
 */
class Bebe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bebe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechnac'], 'safe'],
            [['status'], 'integer'],
            [['nombre', 'apPaterno', 'apMaterno'], 'string', 'max' => 45],
            [['observaciones'], 'string', 'max' => 500],
            [['colonia', 'delegacion'], 'string', 'max' => 30],
            [['cp'], 'string', 'max' => 10],
            [['sexo'], 'string', 'max' => 1],
            [['entero'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBebe' => 'Id Bebe',
            'nombre' => 'Nombre',
            'apPaterno' => 'Ap Paterno',
            'apMaterno' => 'Ap Materno',
            'fechnac' => 'Fechnac',
            'observaciones' => 'Observaciones',
            'colonia' => 'Colonia',
            'delegacion' => 'Delegacion',
            'cp' => 'Cp',
            'sexo' => 'Sexo',
            'status' => 'Status',
            'entero' => 'Entero',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBebeContactos()
    {
        return $this->hasMany(BebeContacto::className(), ['idBebe' => 'idBebe']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['idBebe' => 'idBebe']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlamadas()
    {
        return $this->hasMany(Llamadas::className(), ['idBebe' => 'idBebe']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObscasas()
    {
        return $this->hasMany(Obscasa::className(), ['idBebe' => 'idBebe']);
    }
}
