<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bebes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bebe-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bebe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idBebe',
            'nombre',
            'apPaterno',
            'apMaterno',
            'fechnac',
            // 'observaciones',
            // 'colonia',
            // 'delegacion',
            // 'cp',
            // 'sexo',
            // 'status',
            // 'entero',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
