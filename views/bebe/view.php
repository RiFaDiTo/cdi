<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bebe */

$this->title = $model->idBebe;
$this->params['breadcrumbs'][] = ['label' => 'Bebes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bebe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idBebe], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idBebe], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idBebe',
            'nombre',
            'apPaterno',
            'apMaterno',
            'fechnac',
            'observaciones',
            'colonia',
            'delegacion',
            'cp',
            'sexo',
            'status',
            'entero',
        ],
    ]) ?>

</div>
