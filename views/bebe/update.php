<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bebe */

$this->title = 'Update Bebe: ' . ' ' . $model->idBebe;
$this->params['breadcrumbs'][] = ['label' => 'Bebes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idBebe, 'url' => ['view', 'id' => $model->idBebe]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bebe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
